<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function() {

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/save-url', 'HomeController@urlSave')->name('saveUrl');
Route::get('/create-url', 'HomeController@urlCreate')->name('create_url');

});





Route::group(['prefix'=>''],function(){    	
  	Route::any('/{url?}/{id?}',['as'=>'index', 'uses' =>'urlController@index']); 

});
