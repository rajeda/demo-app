<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Link extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'url','short_url','shorturl_counter'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */



}
