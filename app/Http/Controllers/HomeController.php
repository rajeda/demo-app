<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        $this->middleware('auth');
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $urls = $this->getUrls(); 

        $count_duplicats = 0;

        foreach($urls as $url) {

           $count_duplicats = $count_duplicats + $url->shorturl_counter;
        }
        return view('home',compact('urls','count_duplicats'));
    }

    public function urlCreate() {

        $urls = $this->getUrls();
        return view('duplicate_url',compact('urls'));
    }

    public function urlSave(Request $request){


         //check url exist or not

        $check_url = $this->urlExist($request->url);

        if($check_url) {

        //check url exist in db
        $check_db = $this->urlExistDB($request->url);

        if($check_db == 0) {

          //not exist create

                $link_table = new Link;

                $link_table->user_id =Auth::user()->id;
                $link_table->title = $request->title;
                $link_table->url = $request->url;
                $link_table->short_url =  $this->random_num(7);
                $link_table->shorturl_counter = 1;
                $link_table->created_at = date('Y-m-d H:i:s');

                if($link_table->save()){

                    return $this->getUrls();

                }else{
                    return 0;
                } 
        }else{

              //exist update

              $link_table = Link::find($check_db);

              $link_table->title = $request->title;
              $link_table->short_url = $this->random_num(7);
              $link_table->shorturl_counter = $link_table->shorturl_counter + 1;

              if($link_table->save()){

                      return $this->getUrls();

             }else{
                      return 0;
             } 
        }

        }else{

                return 2;
        }



    }

    public function urlExist($url) {
            
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_NOBODY, true);
            $result = curl_exec($curl);
            if ($result !== false) 
            {
              $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
              if ($statusCode == 404) 
              {
                return false;
              }
              else
              {
                 return true;
              } 
            }
            else
            {
              return false;
            }


    }

    public function urlExistDB($url) {

        $url_exist = Link::where('url',$url)->get();

        if(count($url_exist) > 0) {

          foreach($url_exist as $ue){

            $id = $ue->id;
          }

        }else{

          $id =0;

        }

        return $id;
    }


    public function getUrls() {

        //get shortent url result of the user
        $userid = Auth::user()->id;
        $urls = Link::where('user_id',$userid)
                                ->orderBy('id','desc')
                                ->get();

        return $urls;
    }

     public function random_num($size) {
  $alpha_key = '';
  $keys = range('A', 'Z');

  for ($i = 0; $i < 2; $i++) {
    $alpha_key .= $keys[array_rand($keys)];
  }

  $length = $size - 2;

  $key = '';
  $keys = range(0, 9);

  for ($i = 0; $i < $length; $i++) {
    $key .= $keys[array_rand($keys)];
  }

  return $alpha_key . $key;
}

}
