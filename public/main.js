var main = (function() {
	function ValidURL(str) {
	   regexp =  /^(?:(?:http?|ftp?|https):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        if (regexp.test(str))
        {
          return true;
        }
        else
        {
          return false;
        }
	}
	
	//validate fields

	function validate(){
		var check = 0;
		
		//check valid url 

		if($("#title").val() == "") {
			$(".title_error").text("Please Enter Title");
			check = 1;
		}

		if($("#url").val() == "") {
			$(".url_error").text("Please Enter Url");
			check = 1;
		}else{

			if(!ValidURL($("#url").val())) {

				//not valid url

				$(".url_error").text("Please Enter A Valid Url");
				check = 1;

			}

		}

		

		if(check == 1){
			return false;
		}else{
			return true;
		}
	}
	
	//ajax data save

	function insert(url,data) {

		//validate form

	
				return $.ajax({
						
						url : url ,
						data : data,
						type : "GET",
						dataType : 'json'
				});

		
	

		

	}
	
	//create tiny url

	function createUrl(url,data) {

			if(validate(data)){
			$("#submt_btn").text("Saving....");
		
		var insertUrl = insert(url,data);

		insertUrl.done(function(res) {
			 $("#submt_btn").text("Submit");
			if(res == 0) {
				alert("failed to create url");
			}
			else if(res == 2){
				alert("Url  Not Exist");
			}
			else{
				$(".message-success").show();
				$(".message-success").html('<span class="message_show">Successfully created url</span>');

			 if(res.length > 0) {

			 				var html = "";
							var count = 1;
							var base_url = main_url;
							$.each(res,function(i) {
								
					
								html += "<tr>";
								html += "<td>"+count+"</td>";
								html += "<td>"+res[i].title+"</td>";
								html += "<td>"+res[i].url+"</td>";
								html += "<td>"+base_url+"/"+res[i].short_url+"</td>";
								html += "<td>"+res[i].created_at+"</td>";
								html += "<td><button class='btn btn-primary btn-copy' onclick='copyLink(this)'>Copy Url</button></td>";
								count += 1;
							});

							$("#url_tbdy").html(html);
							
							$("#formTiny")[0].reset();

			 }
			

			}


			
		}).fail(function(xhr, status, errorThrown){
			alert( "Sorry, there was a problem!" );
		    console.log( "Error: " + errorThrown );
		    console.log( "Status: " + status );
		    console.dir( xhr );
		});



}
				
	}

	return {

		createUrls : createUrl
	}

}());