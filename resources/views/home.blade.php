@extends('layouts.app')

@section('content')
<style>
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    width: 40%;
    display: inline-block;
}

.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}


</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Tiny Url - Dashboard
                    
                <a href="{{URL::route('create_url')}}" class="pull-right btn btn-primary">Shorten New URL</a>
                </div>

                <div class="panel-body">
                     @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                  
                       
                </div>
            </div>
        </div>

         <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"></div>

                <div class="panel-body">
                   <div class="card">
                 
                  <div class="container">
                    <h4><b>TOTAL URLS</b></h4> 
                    <h3>{{count($urls)}}</h3> 
                  </div>
                </div>


                 <div class="card">
                  
                  <div class="container">
                    <h4><b>DUPLICATE URLS</b></h4> 
                     <h3>{{$count_duplicats}}</h3
                  </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
