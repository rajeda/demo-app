@extends('layouts.app')

@section('content')
<style>
    table { table-layout: fixed; }
table td { word-wrap: break-word; }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Create Tiny Url

                     <a href="{{URL::route('home')}}" class="pull-right btn btn-primary">Dashboard</a>

                 </div>
                
                <div class="panel-body">
                   <!--  @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in! -->
                         <div class="alert alert-success message-success" style="display: none">
                            
                        </div>
                        <div class="alert alert-danger message-failed"  style="display: none">
                            
                        </div>
                    <form id="formTiny">
                        <div class="form-group">
                            <label for="">Title</label>
                            <input type="text" class="form-control input-change" id="title">
                             <span class="title_error text-danger error"></span>
                        </div>
                          <div class="form-group">
                            <label for="">Url</label>
                            <input type="url" class="form-control input-change" id="url">
                            <span class="url_error text-danger error"></span>
                        </div>
                        <button id="submt_btn" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>

         <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">List Url Short</div>

                <div class="panel-body">
                    <table class="table table-responsive">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Url</th>
                            <th>Short Url</th>
                            <th>Created Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody id="url_tbdy">
                                @if(count($urls) > 0)
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($urls as $url)

                                        <tr>
                                            
                                            <td>{{$i}}</td>
                                            <td>{{$url->title}}</td>
                                            <td>{{$url->url}}</td>
                                            <td>{{url($url->short_url)}}</td>
                                            <td>{{$url->created_at}}</td>
                                            <td>
                                                <button class="btn btn-primary btn-copy" onclick="copyLink(this)">Copy Url</button>
                                            </td>
                                        </tr>
                                     @php
                                        $i = $i+1;
                                    @endphp
                                    @endforeach

                                @else

                                @endif
                        </tbody>

                    </table>
                  
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var main_url = "{{URL::to('/')}}";
</script>
<script src="{{ asset('public/main.js') }}"></script>


<script>
    
    
    $("#submt_btn").on('click',function(e){
    e.preventDefault();
    var url = "{{URL::route('saveUrl')}}";
    var data = {
        
        title : $("#title").val(),
        url : $("#url").val(),

    };
    $(".error").text("");
    main.createUrls(url,data);


    });

    setInterval(function() {
        $(".message-success").hide();
        $(".message-success").html("");

        
    },15000)

   $(".message-success").on('mouseover',function() {
            
            $(this).hide();
            $(this).html("");

   });


   
    

function copyLink(e){
 var msg = $(e).parent().parent().find('td:eq(3)').text();

       // alert(msg);
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(msg).select();
    document.execCommand("copy");
    $temp.remove();


}





</script>

@endsection
